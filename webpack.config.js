'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const nodeModulesPath = path.join(__dirname, 'node_modules');

module.exports = {
  devtool: 'eval-source-map',
  entry: [
    'webpack-hot-middleware/client?reload=true',
    path.join(__dirname, 'src', 'app', 'index')
  ],
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: '[name].js',
    publicPath: '/'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/app/index.html',
      inject: 'body',
      filename: 'index.html'
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    })
  ],
  module: {
    loaders: [{
      test: /\.tsx?$/,
      loader: 'babel?presets[]=es2015,presets[]=stage-0,presets[]=react,presets[]=react-hmre' +
      '!ts-loader',
      include: path.resolve(__dirname, 'src', 'app')
    }, {
      test: /\.json?$/,
      loader: 'json'
    }, {
      test: /\.css$/,
      loader: 'style!css?modules&localIdentName=[name]---[local]---[hash:base64:5]'
    }]
  },
  resolveLoader: {
    root: nodeModulesPath
  },
  resolve: {
    extensions: ['', '.tsx', '.ts', '.js', '.less', '.css'],
    modulesDirectories: ['node_modules', 'resources'],
    alias: {
      'styles.css': path.join(__dirname, 'app', 'styles.css'),
      'pokemons': path.join(nodeModulesPath, 'pokemon-go-node-api', 'pokemons.json'),
      'items': path.join(__dirname, 'items.json')
    }
  }
};
