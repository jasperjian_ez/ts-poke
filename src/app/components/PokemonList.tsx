import * as React from 'react'
import {Dispatch} from 'redux'
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux'
import * as POGOProtos from 'node-pogo-protos';
import * as pokemons from 'pokemons';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

import { store } from '../index';
import PokemonListItem from './PokemonListItem';
import {favorite, transport} from '../actions'

interface PokemonListProps extends MapStateProps, MapDispatchProps {
}

interface PokemonState {
    pokemons: POGOProtos.Data.PokemonData[];
}

export class PokemonList extends React.Component<PokemonListProps, PokemonState> {
    componentWillMount() {
        store.subscribe(() => {
            this.setState({ pokemons: store.getState().inventory.pokemon });
        });
    }

    render() {
        const {pokemons} = this.props;

        return (
            <List>
                <Subheader style={{ textAlign: 'right' }}></Subheader>
                {
                    pokemons.map((pokemon, i) => {
                        return (
                            <div key={pokemon.id}>
                                <PokemonListItem key={pokemon.id.toString() } pokemon={pokemon}
                                    {...pokemons}
                                    onTransport={() => this.props.onTransport(pokemon) }
                                    onFavorite={() => this.props.onFavorite(pokemon) }
                                    />
                                <Divider  inset={true} />
                            </div>
                        );
                    })
                }
            </List>
        );
    }
}

interface MapDispatchProps {
    onFavorite: (pokemon: POGOProtos.Data.PokemonData) => void;
    onTransport: (pokemon: POGOProtos.Data.PokemonData) => void;
}

const mapDispatchToProps: MapDispatchToPropsFunction<MapDispatchProps, {}> = (dispatch) => {
    return {
        onFavorite: (pokemon: POGOProtos.Data.PokemonData) => {
            dispatch(favorite(pokemon));
        },
        onTransport: (pokemon: POGOProtos.Data.PokemonData) => {
            dispatch(transport(pokemon));
        }
    };
}

interface MapStateProps {
    pokemons: POGOProtos.Data.PokemonData[];
}

const mapStateToProps: MapStateToProps<MapStateProps, PokemonListProps> = state => {
    return {
        pokemons: state.inventory.pokemon
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PokemonList);