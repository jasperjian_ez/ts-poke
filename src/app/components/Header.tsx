import * as React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import SocialPerson from 'material-ui/svg-icons/social/person';

export default class Header extends React.Component<{}, {}>{
    _onRightClick(e: React.TouchEvent) {
    }

    render() {
        return (
            <AppBar title="ts-poke" iconElementLeft={<IconButton onTouchTap={this._onRightClick}><SocialPerson /></IconButton>} />
        );
    }
}