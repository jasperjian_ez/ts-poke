import * as React from 'react'
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import Dialog from 'material-ui/Dialog';
import Slider from 'material-ui/Slider';

import * as _ from 'lodash';
import * as POGOProtos from 'node-pogo-protos';
import * as items from 'items';

interface StorageListItemProps {
    item: POGOProtos.Inventory.Item.ItemData;
    onDelete?: (item: POGOProtos.Inventory.Item.ItemData, count: number) => void;
}

interface StorageListItemState {
    open?: boolean,
    max?: number;
    value?: number;
}

export default class PokemonListItem extends React.Component<StorageListItemProps, StorageListItemState> {
    state = {
        open: false,
        max: 1,
        value: 1
    }

    handleDelete() {
        this.setState({
            open: true,
            max: this.props.item.count,
            value: 1
        });
    }

    handleDeleteSubmit() {
        this.props.onDelete(this.props.item, this.state.value);
        this.handleClose();
    }

    handleClose() {
        this.setState({ open: false });
    };

    handleChange(value: number) {
        this.setState({ value: value });
    }

    render() {
        const {item} = this.props;
        const itemInfo = _.find(items.items, i => i.id === item.item_id);

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose.bind(this) }
                />,
            <FlatButton
                disabled={this.state.value === 0}
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleDeleteSubmit.bind(this) }
                />,
        ];

        return (
            <div key={item.item_id}>
                <ListItem ref="test"
                    leftAvatar={
                        <Avatar
                            src={itemInfo.img}
                            />
                    }
                    primaryText={itemInfo.name}
                    secondaryText={
                        <p>
                            {item.count}
                        </p>
                    }
                    secondaryTextLines={2}
                    rightIconButton={
                        item.unseen ? null :
                            <IconButton onTouchTap={this.handleDelete.bind(this) }>
                                <FontIcon className="material-icons">delete </FontIcon>
                            </IconButton>
                    }
                    />
                <Dialog
                    title={`Discard ${itemInfo.name}?`}
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose.bind(this) }
                    >
                    <Slider step={1} value={this.state.value} onChange={(e, v) => this.handleChange(v) } max={this.state.max} min={0} />
                    {this.state.value}/{this.state.max}
                </Dialog>
            </div>
        );
    }
}