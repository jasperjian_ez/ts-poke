import * as React from 'react'
import {Dispatch} from 'redux'
import * as ReactDOM from 'react-dom';
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';
import {deepOrange500} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import * as POGOProtos from 'node-pogo-protos';
import * as pokemons from 'pokemons';
import * as Api from 'App';
import { catchPokemon, loot } from '../actions';

let map: google.maps.Map;
let infowindow: google.maps.InfoWindow;

const mapStyles: React.CSSProperties = {
    height: '100%'
}

const muiTheme = getMuiTheme({
    palette: {
        accent1Color: deepOrange500,
    },
});

interface MarkerProps {
    catchablePokemon: POGOProtos.Map.Pokemon.MapPokemon;
    onCatch: (catchablePokemon: POGOProtos.Map.Pokemon.MapPokemon) => void;
}

interface PokeStopProps {
    fort: POGOProtos.Map.Fort.FortData;
    onLoot: (fort: POGOProtos.Map.Fort.FortData) => any;
}

interface PokeStopState {
    open: boolean;
    lootResult?: POGOProtos.Networking.Responses.FortSearchResponseData;
}

export class PokeStop extends React.Component<PokeStopProps, PokeStopState>{
    state = {
        open: false,
        lootResult: {} as any
    };

    handleLoot() {
        this.props.onLoot(this.props.fort);
    }

    handleRequestClose() {
        this.setState({
            open: false,
        });
    }

    render() {
        let {fort} = this.props;
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div>
                    <Card>
                        <CardHeader
                            title={fort.sponsor}
                            subtitle={fort.active_fort_modifier}
                            // avatar={'pokemon.img'}
                            />
                        <CardActions>
                            <FlatButton label="Loot" onTouchTap={e => this.handleLoot() } />
                        </CardActions>
                    </Card>
                    {
                        // <Snackbar
                        // open={this.state.open}
                        // message={this.state.lootResult.result}
                        // autoHideDuration={4000}
                        // onRequestClose={this.handleRequestClose}
                        // />
                    }
                </div>
            </MuiThemeProvider>
        );
    }
}


export class Marker extends React.Component<MarkerProps, {}>{
    handleCatch() {
        $.ajax({
            url: '/api/player/pokemon/catch',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ catchablePokemon: this.props.catchablePokemon })
        }).then((data, status) => {
            debugger;
        });
    }
    render() {
        let {catchablePokemon} = this.props;
        let pokemonData = pokemons.pokemon[catchablePokemon.pokemon_id - 1];
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <Card>
                    <CardHeader
                        title={pokemonData.name}
                        // subtitle={`${Math.floor(+catchablePokemon.expiration_timestamp_ms / 1000 / 60)} mins`}
                        avatar = {pokemonData.img}
                        />
                    <CardActions>
                        <FlatButton label="Catch" onTouchTap={e => this.props.onCatch(catchablePokemon) } />
                    </CardActions>
                </Card>
            </MuiThemeProvider>
        );
    }
}

interface MapProps {
    isCatching: boolean;
    catchResult: any;
    onCatch: (catchablePokemon: POGOProtos.Map.Pokemon.MapPokemon) => any;
    onLoot: (fort: POGOProtos.Map.Fort.FortData) => any;
}

interface MapState {
    open: boolean;
    message: string;
}

export class Map extends React.Component<MapProps, MapState> {
    state = {
        open: false,
        message: ''
    };

    componentDidMount() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 25.064132,
                lng: 121.540328
            },
            zoom: 18
        });

        map.addListener('idle', () => {
            let center = map.getCenter();

            $.getJSON(`/api/map/nearby/${center.lat()},${center.lng()}`).done(this.createMarker.bind(this));
        });

        let a = 1;
    }

    handleCatch(pokemon: POGOProtos.Map.Pokemon.MapPokemon) {
        this.props.onCatch(pokemon);
    }

    handleRequestClose() {
        // this.setState({
        //     open: false,
        //     message: ''
        // });
    }

    createMarker(data: Api.NearbyResponse) {
        data.pokestops
            .forEach(fort => {
                let marker = new google.maps.Marker({
                    map: map,
                    position: {
                        lat: fort.latitude,
                        lng: fort.longitude
                    },
                    icon: 'http://maps.google.com/mapfiles/kml/pal2/icon5.png'
                });

                infowindow = new google.maps.InfoWindow();

                marker.addListener('click', () => {
                    let div = document.createElement('div');
                    ReactDOM.render(<PokeStop fort={fort} onLoot={this.props.onLoot.bind(this) } />, div)
                    infowindow.setContent(div);
                    infowindow.open(map, marker);
                });
            });

        data.catchablePokemons.forEach((catchablePokemon, j) => {
            let pokemonData = pokemons.pokemon[catchablePokemon.pokemon_id - 1];
            let marker = new google.maps.Marker({
                map: map,
                position: {
                    lat: catchablePokemon.latitude,
                    lng: catchablePokemon.longitude
                },
                title: `${catchablePokemon.pokemon_id}`
            });

            if (pokemonData) {
                marker.setTitle(pokemonData.name);
            }

            infowindow = new google.maps.InfoWindow();

            marker.addListener('click', () => {
                let div = document.createElement('div');
                ReactDOM.render(<Marker catchablePokemon={catchablePokemon} onCatch={this.handleCatch.bind(this) } />, div)
                infowindow.setContent(div);
                infowindow.open(map, marker);
            });
        });
    }

    render() {
        let msg = '';
        if (!this.props.isCatching) {
            msg = 'Catching...';
        }
        else {
            if (this.props.catchResult.result === POGOProtos.Networking.Responses.CatchPokemonResponse.CatchStatus.CATCH_SUCCESS) {
                msg = 'Success';
            }
            else {
                msg = 'Fail';
            }
        }

        return (
            <div id="map" style={mapStyles}></div>
        );
    }
}

const mapDispatchToProps: MapDispatchToPropsFunction<{}, {}> = (dispatch) => {
    return {
        onCatch: (pokemon: POGOProtos.Map.Pokemon.MapPokemon) => {
            dispatch(catchPokemon(pokemon));
        },
        onLoot: (fort: POGOProtos.Map.Fort.FortData) => {
            dispatch(loot(fort));
        }
    };
}

const mapStateToProps: MapStateToProps<MapProps, {}> = state => {
    return {
        isCatching: state.inventory.isCatching,
        catchResult: state.inventory.catchResult
    } as MapProps;
};

export default connect(mapStateToProps, mapDispatchToProps)(Map);