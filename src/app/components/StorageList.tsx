import * as React from 'react'
import {Dispatch} from 'redux'
import { connect, MapStateToProps, MapDispatchToPropsFunction } from 'react-redux'
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

import * as POGOProtos from 'node-pogo-protos';
import * as pokemons from 'pokemons';
import { store } from '../index';
import StorageListItem from './StorageListItem';
import { deleteItem } from '../actions'

interface StorageListProps extends MapStateProps, MapDispatchProps {
}

interface StorageListState {
    items: POGOProtos.Inventory.Item.ItemData[];
}

export class StorageList extends React.Component<StorageListProps, StorageListState> {
    componentWillMount() {
        store.subscribe(() => {
            this.setState({ items: store.getState().inventory.items });
        });
    }

    render() {
        const {items} = this.props;

        return (
            <List>
                <Subheader style={{ textAlign: 'right' }}></Subheader>
                {
                    items.map((item, i) => {
                        return (
                            <div key={item.item_id}>
                                <StorageListItem key={item.item_id} item={item}
                                    {...items}
                                    onDelete={this.props.onDelete}
                                    />
                                <Divider  inset={true} />
                            </div>
                        );
                    })
                }
            </List>
        );
    }
}

interface MapDispatchProps {
    onDelete: (item: POGOProtos.Inventory.Item.ItemData, count: number) => void;
}

const mapDispatchToProps: MapDispatchToPropsFunction<MapDispatchProps, {}> = (dispatch) => {
    return {
        onDelete: (item: POGOProtos.Inventory.Item.ItemData, count: number) => {
            dispatch(deleteItem(item, count));
        }
    };
}

interface MapStateProps {
    items: POGOProtos.Inventory.Item.ItemData[];
}

const mapStateToProps: MapStateToProps<MapStateProps, {}> = state => {
    return {
        items: state.inventory.items
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(StorageList);