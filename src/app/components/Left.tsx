import * as React from 'react';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

import * as pogobuf from 'pogobuf';
import * as POGOProtos from 'node-pogo-protos';

import PlayerInfo from './PlayerInfo';

interface LeftProp {
    open: boolean;
    onClose(open: boolean): void;
    pokemons: POGOProtos.Data.PokemonData[];
    inventory: pogobuf.Utils.Inventory;
}

interface LeftState {
    open: boolean;
}

export default class Left extends React.Component<LeftProp, LeftState>{
    constructor(props) {
        super(props);
        this.state = {
            open: this.props.open
        }
    }
    close(e: React.MouseEvent) {
        this.props.onClose(false);
    }
    render() {
        const { open, pokemons, inventory } = this.props;

        return (
            <Drawer open={open}>
                <AppBar title="" iconElementLeft={<i/>} iconElementRight={<IconButton onClick={e => this.close(e) }><FontIcon className="material-icons">close</FontIcon></IconButton>} />
                <PlayerInfo inventory={inventory} />
            </Drawer>
        );
    }
}