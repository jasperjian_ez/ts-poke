import * as React from 'react'
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { grey400 } from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog';

import * as POGOProtos from 'node-pogo-protos';
import * as pokemons from 'pokemons';
import * as items from 'items';

const iconButtonElement = (
    <IconButton
        touch={true}
        tooltip="more"
        tooltipPosition="bottom-left"
        >
        <MoreVertIcon color={grey400} />
    </IconButton>
);

const rightIconMenu = (pokemon: POGOProtos.Data.PokemonData, onTransport, onFavorite) => {
    let startIcon;

    if (pokemon.favorite) {
        startIcon = <FontIcon className="material-icons">star</FontIcon>
    }
    else {
        startIcon = <FontIcon className="material-icons">star_border</FontIcon>
    }

    return (
        <IconMenu iconButtonElement={iconButtonElement}>
            <MenuItem
                onTouchTap={onTransport}
                leftIcon={<FontIcon className="material-icons">transform</FontIcon>}>Transfer</MenuItem>
            <MenuItem
                onTouchTap={onFavorite}
                leftIcon={startIcon}>Favorite</MenuItem>
                <MenuItem
                leftIcon={<FontIcon className="material-icons">exposure_plus_1</FontIcon>}>Power Up</MenuItem>
                <MenuItem
                leftIcon={<FontIcon className="material-icons">trending_up</FontIcon>}>Envolve</MenuItem>
        </IconMenu>
    );
};

interface PokemonListItemProps {
    pokemon: POGOProtos.Data.PokemonData;
    onTransport: (pokemon: POGOProtos.Data.PokemonData) => void;
    onFavorite: (pokemon: POGOProtos.Data.PokemonData) => void;
}

export default class PokemonListItem extends React.Component<PokemonListItemProps, { open: boolean }> {
    state = {
        open: false
    }

    handleTransport() {
        this.setState({ open: true });
    }

    handleTransportSubmit() {
        this.props.onTransport(this.props.pokemon);
        this.handleClose();
    }

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const {pokemon, onTransport, onFavorite} = this.props;
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose.bind(this)}
                />,
            <FlatButton
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleTransportSubmit.bind(this)}
                />,
        ];

        let listItem: JSX.Element;

        if (pokemon.pokemon_id > 0) {
            let pokemonData = pokemons.pokemon[pokemon.pokemon_id - 1];
            listItem = (
                <div key={pokemon.id}>
                <ListItem
                    leftAvatar={<Avatar
                        src={pokemonData.img}
                         />}
                    primaryText={pokemon.nickname || pokemonData.name}
                    secondaryText={
                        <p>
                            cp: {pokemon.cp} iv: {pokemon.individual_attack}, {pokemon.individual_defense}, {pokemon.individual_stamina}
                        </p>
                    }
                    secondaryTextLines={2}
                    rightIconButton={rightIconMenu(pokemon, this.handleTransport.bind(this), onFavorite) }
                    />
                <Dialog
                    title={`Do you want to transfer ${pokemon.nickname || pokemonData.name} to the Professor?`}
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                    >
                    You can't undo this action once you transfer.
                </Dialog>
            </div>
            );
        }
        else {
            let egg = items.items[0];
            listItem = (
                <ListItem
                    leftAvatar={<Avatar
                        src={egg.img}
                         />}
                    primaryText={egg.name}
                    secondaryText={
                        <p>
                            {pokemon.egg_km_walked_target} KM
                        </p>
                    }
                    secondaryTextLines={2}
                    />
            );
        }

        return (
            listItem
        );
    }
}