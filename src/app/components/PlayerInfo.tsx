import * as React from 'react';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import { List, ListItem } from 'material-ui/List';
import { Tabs, Tab } from 'material-ui/Tabs';
import LinearProgress from 'material-ui/LinearProgress';
import FontIcon from 'material-ui/FontIcon';

import * as _ from 'lodash';
import * as pogobuf from 'pogobuf';
import * as POGOProtos from 'node-pogo-protos';
import PokemonList from './PokemonList';
import StorageList from './StorageList';

interface PlayerInfoProps {
    inventory: pogobuf.Utils.Inventory;
}

interface PlayerInfoState {
    profile?: POGOProtos.Data.PlayerData;
}

const styles = {
    headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
    },
};

interface PlayerData extends POGOProtos.Data.PlayerData, POGOProtos.Data.Player.PlayerStatsData {

}

export default class PlayerInfo extends React.Component<PlayerInfoProps, PlayerInfoState>{
    private res: JQueryXHR;
    state = {
        profile: {} as PlayerData
    }

    componentDidMount() {
        this.res = $.get('/api/player').done((data: PlayerData) => {
            this.setState({
                profile: data
            });
        }) as JQueryXHR;
    }

    componentWillUnmount() {
        this.res.abort();
    }

    render() {
        const { inventory } = this.props;
        const { profile } = this.state;

        let pokemonStorage = inventory.pokemon.length;
        let itemStorage = _.sumBy(inventory.items, item => item.count);
        let exp: number = profile.experience as any;
        let nextLvExp: number = profile.next_level_xp as any;

        return (
            <div>
                <Card>
                    <CardHeader
                        title={profile.username}
                        subtitle={'lv. ' + profile.level}
                        avatar=""
                        />
                    <CardText>
                        Xp ({exp}/{nextLvExp})
                        <LinearProgress mode="determinate" max={nextLvExp} value={exp} />
                        Poke Storge ({pokemonStorage}/{profile.max_pokemon_storage})
                        <LinearProgress mode="determinate" max={profile.max_pokemon_storage} value={pokemonStorage} />
                        Item Storage ({itemStorage}/{profile.max_item_storage})
                        <LinearProgress mode="determinate" max={profile.max_item_storage} value={itemStorage} />
                    </CardText>
                </Card>
                <Tabs>
                    <Tab
                        icon={<FontIcon className="material-icons">pets</FontIcon>}
                        label="">
                        <PokemonList></PokemonList>
                    </Tab>
                    <Tab icon={<FontIcon className="material-icons">storage</FontIcon>}
                        label="">
                        <StorageList></StorageList>
                    </Tab>
                </Tabs>
            </div>
        );
    }
}