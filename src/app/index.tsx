import * as React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import * as createLogger from 'redux-logger'
import reducer from './reducers'
import App from './containers/App'

import * as injectTapEventPlugin from 'react-tap-event-plugin';

import * as pogobuf from 'pogobuf';


injectTapEventPlugin();

const middleware: any[] = [ thunk ];
if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger())
}

export const store = createStore<{ inventory: pogobuf.Utils.Inventory }>(
  reducer,
  applyMiddleware(...middleware)
)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
);