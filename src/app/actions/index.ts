import {Dispatch} from 'redux';
import { createAction, Action } from 'redux-actions';
import * as POGOProtos from 'node-pogo-protos';
import * as pogobuf from 'pogobuf';
import * as fetch from 'isomorphic-fetch'

export enum ActionTypes {
  FAVORITE_REQUEST,
  FAVORITE_COMPLETE,
  TRANSPORT_REQUEST,
  TRANSPORT_COMPLETE,
  REQUEST_INVENTORY,
  RECEIVE_INVENTORY,
  CATCH_REQUEST,
  CATCH_COMPLETE,
  DELETE_REQUEST,
  DELETE_COMPLETE,
  LOOT_REQUEST,
  LOOT_COMPLETE,
}

export interface PokemonAction {
  type: ActionTypes;
  pokemon: POGOProtos.Data.PokemonData;
}

export interface InventoryAction {
  type: ActionTypes;
  result: any;
}

const favoriteRequest = () => ({
  type: ActionTypes.FAVORITE_REQUEST
});

const favoriteComplete = (pokemon: POGOProtos.Data.PokemonData) => ({
  type: ActionTypes.FAVORITE_COMPLETE,
  result: {
    pokemon: pokemon
  }
});

const favorite = (pokemon: POGOProtos.Data.PokemonData) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(favoriteRequest())

    const payload = {
      pokemon: pokemon
    };

    fetch(`/api/player/pokemon/favorite`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
      .then(res => res.json<POGOProtos.Networking.Responses.SetFavoritePokemonResponse.Result>())
      .then(result => {
        if (result === POGOProtos.Networking.Responses.SetFavoritePokemonResponse.Result.SUCCESS) {
          dispatch(favoriteComplete(pokemon));
        }
      });
  }
}

const transportRequest = () => ({
  type: ActionTypes.TRANSPORT_REQUEST
});

const transportComplete = (pokemon: POGOProtos.Data.PokemonData) => ({
  type: ActionTypes.TRANSPORT_COMPLETE,
  result: {
    pokemon: pokemon
  },
  receivedAt: Date.now()
});

const transport = (pokemon: POGOProtos.Data.PokemonData) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(transportRequest())

    const payload = {
      pokemon: pokemon
    };

    fetch(`/api/player/pokemon/transport`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
      .then(res => res.json<POGOProtos.Networking.Responses.ReleasePokemonResponse.Result>())
      .then(result => {
        if (result === POGOProtos.Networking.Responses.ReleasePokemonResponse.Result.SUCCESS) {
          dispatch(transportComplete(pokemon));
        }
      });
  }
}

const catchRequest = () => ({
  type: ActionTypes.CATCH_REQUEST
});

const catchComplete = (pokemon: POGOProtos.Data.PokemonData) => ({
  type: ActionTypes.CATCH_COMPLETE,
  result: { pokemon: pokemon },
  receivedAt: Date.now()
});

interface CatchPokemonResponse {
  status: POGOProtos.Networking.Responses.CatchPokemonResponse.CatchStatus,
  pokemon: POGOProtos.Data.PokemonData;
}

const catchPokemon = (pokemon: POGOProtos.Map.Pokemon.MapPokemon) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(catchRequest())

    const payload = {
      catchablePokemon: pokemon
    };

    fetch(`/api/player/pokemon/catch`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
      .then(res => res.json<CatchPokemonResponse>())
      .then(result => {
        if (result.status === POGOProtos.Networking.Responses.CatchPokemonResponse.CatchStatus.CATCH_SUCCESS) {
          dispatch(catchComplete(result.pokemon));
        }
      });
  };
}

const deleteRequest = () => ({
  type: ActionTypes.DELETE_REQUEST
});

const deleteComplete = (item: POGOProtos.Inventory.Item.ItemData, newCount: number) => ({
  type: ActionTypes.DELETE_COMPLETE,
  result: { item: item, newCount: newCount },
  receivedAt: Date.now()
});

const deleteItem = (item: POGOProtos.Inventory.Item.ItemData, count: number) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(catchRequest())

    const payload = {
      itemId: item.item_id,
      count: count
    };

    fetch(`/api/player/inventory/delete`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
      .then(res => res.json<POGOProtos.Networking.Responses.RecycleInventoryItemResponse>())
      .then(result => {
        if (result.result === POGOProtos.Networking.Responses.RecycleInventoryItemResponse.Result.SUCCESS) {
          dispatch(deleteComplete(item, result.new_count));
        }
      });
  };
};

const lootRequest = () => ({
  type: ActionTypes.LOOT_REQUEST
});

const lootComplete = (itemAwards: POGOProtos.Inventory.Item.ItemAward[]) => ({
  type: ActionTypes.LOOT_COMPLETE,
  result: { itemAwards: itemAwards },
  receivedAt: Date.now()
});

const loot = (fort: POGOProtos.Map.Fort.FortData) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(lootRequest())

    const payload = {
      fort: fort
    };

    fetch(`/api/player/pokestop/loot`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
      .then(res => res.json<POGOProtos.Networking.Responses.FortSearchResponse>())
      .then(result => {
        if (result.result === POGOProtos.Networking.Responses.FortSearchResponse.Result.SUCCESS) {
          dispatch(lootComplete(result.items_awarded));
        }
      });
  };
};

function requestInventory() {
  return {
    type: ActionTypes.REQUEST_INVENTORY
  }
}

function receiveInventory(json: pogobuf.Utils.Inventory) {
  return {
    type: ActionTypes.RECEIVE_INVENTORY,
    result: { inventory: json },
    receivedAt: Date.now()
  }
}

function fetchInventory() {
  return dispatch => {
    dispatch(requestInventory())
    return fetch(`/api/player/inventory`)
      .then(response => response.json())
      .then((json) => dispatch(receiveInventory(json)))
  }
}

export function fetchInventoryIfNeeded() {
  return (dispatch: Dispatch<any>, getState) => {
    return dispatch(fetchInventory());
  };
}

export {
fetchInventory,
transport,
favorite,
catchPokemon,
deleteItem,
loot
}