import { combineReducers } from 'redux';

import inventory from './inventory';
import pokemon from './pokemon';
import * as pogobuf from 'pogobuf';

const rootReducer = combineReducers<{ inventory: pogobuf.Utils.Inventory }>({
  inventory,
  pokemon
});

export default rootReducer;
