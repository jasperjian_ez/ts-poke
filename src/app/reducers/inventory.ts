import { handleActions, Action } from 'redux-actions';
import * as pogobuf from 'pogobuf';
import * as POGOProtos from 'node-pogo-protos';
import * as _ from 'lodash';

import { ActionTypes, InventoryAction } from '../actions';
import { Pokemon, AppState } from './model';

interface InitialState extends pogobuf.Utils.Inventory {
    isCatching: boolean;
    catchResult: any;
}

export const initialState: InitialState = {
    pokemon: [],
    items: [],
    pokedex: [],
    player: {},
    currency: [],
    camera: {},
    inventory_upgrades: [],
    applied_items: [],
    egg_incubators: [],
    candies: [],
    isCatching: false,
    catchResult: {}
} as InitialState;

const inventoryReducer = (state = initialState, action: InventoryAction): pogobuf.Utils.Inventory => {
    let pokemon: POGOProtos.Data.PokemonData;

    switch (action.type) {
        case ActionTypes.RECEIVE_INVENTORY:
            return _.merge(state, action.result.inventory);
        case ActionTypes.FAVORITE_REQUEST:
            return state;
        case ActionTypes.FAVORITE_COMPLETE:
            pokemon = _.find(state.pokemon, pokemon => pokemon.id === action.result.pokemon.id);
            pokemon.favorite = action.result.pokemon.favorite ? 0 : 1;
            return state;
        case ActionTypes.TRANSPORT_COMPLETE:
            const pokemonIndex = _.findIndex(state.pokemon, pokemon => pokemon.id === action.result.pokemon.id);
            state.pokemon.splice(pokemonIndex, 1);
            return state;
        case ActionTypes.CATCH_COMPLETE:
            state.isCatching = true;
            state.pokemon.push(action.result.pokemon);
            state.catchResult = action.result;
            return state;
        case ActionTypes.DELETE_COMPLETE:
            let newCount = action.result.newCount;
            const itemIndex = _.findIndex(state.items, item => item.item_id == action.result.item.item_id)

            if (newCount === 0) {
                state.items.splice(itemIndex, 1);
            }
            else {
                state.items[itemIndex].count = action.result.newCount;
            }
            return state;
        case ActionTypes.LOOT_COMPLETE:
            let itemAwards = action.result.itemAwards as POGOProtos.Inventory.Item.ItemAward[];
            itemAwards.forEach(itemAward => {
                let item = state.items.find(i => i.item_id === itemAward.item_id);
                if (item) {
                    item.count += itemAward.item_count;
                }
                else {
                    state.items.push({
                        item_id: itemAward.item_id,
                        count: itemAward.item_count
                    } as POGOProtos.Inventory.Item.ItemData);
                }
            });
            return state;
        default:
            return state;
    }
}

export default inventoryReducer


