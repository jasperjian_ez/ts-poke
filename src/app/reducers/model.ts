import * as pogobuf from 'pogobuf';
import * as POGOProtos from 'node-pogo-protos';
export type Pokemon = POGOProtos.Data.PokemonData;

// export type AppState = pogobuf.Utils.Inventory;

export interface AppState extends pogobuf.Utils.Inventory {
}


