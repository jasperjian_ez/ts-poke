import * as React from 'react';
import { Store, createStore } from 'redux';
import { Provider, connect, Dispatch} from 'react-redux';
import { fetchInventoryIfNeeded } from '../actions'

import { deepOrange500 } from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Map from '../components/Map';
import Header from '../components/Header';
import Left from '../components/Left';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import SocialPerson from 'material-ui/svg-icons/social/person';

import * as pogobuf from 'pogobuf';
import * as POGOProtos from 'node-pogo-protos';
import { AppState as RootState, Pokemon } from '../reducers/model';

const styles: Styles = {
  container: {
    // textAlign: 'center',
    // paddingTop: 200,
    display: 'flex',
    flexDirection: 'column',
    height: '100%'
  }
};

interface Styles {
  container: React.CSSProperties
}

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500,
  },
});

const initialState = {};

interface AppState {
  pokemons?: POGOProtos.Data.PokemonData[];
  leftOpen: boolean;
}

interface AppProps extends RootState {
  dispatch?: Dispatch<any>;
  inventory: pogobuf.Utils.Inventory;
  pokemon: POGOProtos.Data.PokemonData[];
}

export class App extends React.Component<AppProps, AppState> {
  constructor(props, context) {
    super(props, context);
    this.state = {
      leftOpen: false
    };
  }
  componentDidMount() {
    const { dispatch } = this.props
    dispatch(fetchInventoryIfNeeded());
  }

  handleTouchTapLeftIconButton() {
    this.setState({
      leftOpen: !this.state.leftOpen,
    });
  }

  render() {
    const { leftOpen } = this.state;
    const { pokemon, inventory, dispatch } = this.props;

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div style={styles.container}>
          <AppBar title="ts-poke" iconElementLeft={<IconButton onClick={e => this.handleTouchTapLeftIconButton() }><SocialPerson /></IconButton>} />
          <Left open={leftOpen}
            onClose={open => this.handleTouchTapLeftIconButton() }
            inventory={inventory}
            pokemons={pokemon} />
          <Map />
        </div>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = state => {
  const {inventory} = state;
  const {pokemon} = state.inventory;

  return {
    pokemon,
    inventory
  };
};

export default connect(mapStateToProps)(App);