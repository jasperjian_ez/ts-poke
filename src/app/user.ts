import * as pogobuf from 'pogobuf';
import * as POGOProtos from 'node-pogo-protos';
import config from '../../config';

export namespace AccountSettings {
    export interface Account {
        username: string;
        password: string;
        location: Location;
        provider: Provider;
    }

    export enum Provider {
        ptc,
        google
    }

    export interface Location {
        type: LocationType;
        name?: string;
        coords?: Coordinates;
    }

    interface Coordinates {
        latitude: number;
        longitude: number;
        altitude?: number;
    }

    export enum LocationType {
        name,
        coords
    }

    export const DEV_ACCOUNT: Account = {
        username: config.username,
        password: config.password,
        location: {
            type: LocationType.coords,
            coords: {
                latitude: 25.058021,
                longitude: 121.540175
            }
        },
        provider: Provider.google
    };
}

export class Client {
    client: pogobuf.Client;
    login: pogobuf.GoogleLogin;

    constructor(private account: AccountSettings.Account) {
        this.login = new pogobuf.GoogleLogin();
        this.client = new pogobuf.Client();
    }

    init() {
        this.login
            .login(this.account.username, this.account.password)
            .then(token => {
                console.log('Logon.');
                this.client.setAuthInfo('google', token);
                this.client.setPosition(this.account.location.coords.latitude, this.account.location.coords.longitude);

                return this.client.init();
            }).then(res => this.ensureTutorialCompleted());
    }

    ensureTutorialCompleted() {
        this.client.getPlayer()
            .then(player => {
                if (player.player_data.tutorial_state.indexOf(POGOProtos.Enums.TutorialState.LEGAL_SCREEN) < 0) {
                    this.completeTutorial();
                }
            });
    }

    private completeTutorial() {
        this.client
            .encounterTutorialComplete(POGOProtos.Enums.PokemonId.CHARMANDER)
            .then(() => {
                let tutorialStates = [];
                tutorialStates.push(POGOProtos.Enums.TutorialState.LEGAL_SCREEN);

                this.client.markTutorialComplete(tutorialStates, false, false)
                    .then(completeRes => {
                        console.log('markTutorialComplete done.');
                    }).catch(err => {
                        console.error('Failed to mark tutorial as complete... ERROR:', err);
                    });
            }).catch(err => {
                console.error('Failed to complete the tutorial... ERROR:', err);
            });
    }

    getInventory() {
        return new Promise<pogobuf.Utils.Inventory>((resolve, reject) => {
            user.client.getInventory(0)
                .then(inventory => {
                    if (!inventory.success) return reject('success=false in inventory response.');

                    let inventoryGroup = pogobuf.Utils.splitInventory(inventory);

                    resolve(inventoryGroup);
                });
        });
    }
}

let user = new Client(AccountSettings.DEV_ACCOUNT);
user.init();

export default user;