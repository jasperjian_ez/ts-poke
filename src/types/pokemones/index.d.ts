declare module 'pokemons' {
    interface Pokemon {
        id: string;
        num: string;
        name: string;
        img: string;
        type: string;
        height: string;
        weight: string;
        candy: string;
        egg: string;
    }

    interface Pokemons {
        thanks: string;
        pokemon: Pokemon[];
    }

    var pokemons: Pokemons;

    // export default pokemons;
    export = pokemons;
    // export pokemons;
}