declare module 'pokemon-go-node-api' {
    interface Pokeio {
        playerInfo: PlayerInfo;
        pokemonlist: Pokemon[];
        constructor();
        init(username: string, password: string, location: Location, provider: string, callback: (err) => void);
        GetProfile(callback: (err, profile: Profile) => void);
        Heartbeat(callback: (err, hb: Heartbeat) => void);
        SetLocation(location: Location, callback: (error, coordinates: Coordinates) => void);
    }

    interface PokeioStatic {
        new (): Pokeio;
    }

    interface PlayerInfo {
        locationName: string;
        latitude: number;
        longitude: number;
        altitude: number;
    }

    interface Profile {
        username: string;
        poke_storage: number;
        item_storage: number;
        currency: { amount: number }[];
    }

    interface Heartbeat {
        cells: Cell[];
    }


    interface S2CellId {
        low: number;
        high: number;
        unsigned: boolean;
    }

    interface AsOfTimeMs {
        low: number;
        high: number;
        unsigned: boolean;
    }

    interface LastModifiedMs {
        low: number;
        high: number;
        unsigned: boolean;
    }

    interface Team {
    }

    interface GuardPokemonId {
    }

    interface GuardPokemonLevel {
    }

    interface GymPoints {
    }

    interface IsInBattle {
    }

    interface ActiveFortModifier {
    }

    interface LureInfo {
    }

    interface CooldownCompleteMs {
    }

    interface Sponsor {
    }

    interface RenderingType {
    }

    interface Fort {
        FortId: string;
        LastModifiedMs: LastModifiedMs;
        Latitude: number;
        Longitude: number;
        Team: Team;
        GuardPokemonId: GuardPokemonId;
        GuardPokemonLevel: GuardPokemonLevel;
        Enabled: boolean;
        FortType: number;
        GymPoints: GymPoints;
        IsInBattle: IsInBattle;
        ActiveFortModifier: ActiveFortModifier;
        LureInfo: LureInfo;
        CooldownCompleteMs: CooldownCompleteMs;
        Sponsor: Sponsor;
        RenderingType: RenderingType;
    }

    interface SpawnPoint {
        Latitude: number;
        Longitude: number;
    }

    interface WildPokemon {
        Latitude: number;
        Longitude: number;
        TimeTillHiddenMs: number;
        pokemon: Pokemon;
    }

    interface Pokemon{
        Id;
        PokemonId: number;
        cp: number;
    }

    interface IsTruncated {
    }

    interface FortSummary {
    }

    interface DecimatedSpawnPoint {
    }

    interface MapPokemon {
        Latitude: number;
        Longitude: number;
        PokedexTypeId: number;
    }

    interface DistanceMeters {
    }

    interface EncounterId {
        low: number;
        high: number;
        unsigned: boolean;
    }

    interface NearbyPokemon {
        PokedexNumber: number;
        DistanceMeters: DistanceMeters;
        EncounterId: EncounterId;
    }

    interface Cell {
        S2CellId: S2CellId;
        AsOfTimeMs: AsOfTimeMs;
        Fort: Array<Fort>;
        SpawnPoint: Array<SpawnPoint>;
        WildPokemon: Array<WildPokemon>;
        IsTruncatedList: IsTruncated;
        FortSummary: Array<FortSummary>;
        DecimatedSpawnPoint: Array<DecimatedSpawnPoint>;
        MapPokemon: Array<MapPokemon>;
        NearbyPokemon: Array<NearbyPokemon>;
    }

    interface Pokemon {
        name: string;
    }

    interface Location {
        type: string;
        name?: string;
        coords?: Coordinates;
    }

    interface Coordinates {
        latitude: number;
        longitude: number;
        altitude?: number;
    }

    export var Pokeio: PokeioStatic;
}