declare module 'App' {
    import * as POGOProtos from 'node-pogo-protos';

    namespace api {
        export interface NearbyResponse {
            pokestops?: POGOProtos.Map.Fort.FortData[];
            gyms?: POGOProtos.Map.Fort.FortData[];
            catchablePokemons?: POGOProtos.Map.Pokemon.MapPokemon[];
        }
    }
    export = api;
}