declare module 'items' {
    interface Item {
        id: number;
        name: string;
        img: string;
    }

    interface Items {
        items: Item[];
    }

    let items: Items;

    export = items;
}