import * as express from 'express';
import user from '../src/app/user';
import * as pogobuf from 'pogobuf';
import * as bluebird from 'bluebird';
import * as POGOProtos from 'node-pogo-protos';
import * as http from "http";
import * as ExpressCore from 'express-serve-static-core';
import * as _ from 'lodash';

const router = express.Router();

router.get('/', (req, res, next) => {
    user.client.getPlayer()
        .then(player => player.player_data)
        .then(playerData => {
            user.client.getInventory()
                .then(inventory =>
                    _.find(inventory.inventory_delta.inventory_items,
                        item => !!item.inventory_item_data.player_stats).inventory_item_data.player_stats)
                .then(playerStatus => {
                    res.json(_.merge(playerData, playerStatus));
                });

        });
});

router.get('/delta', (req, res, next) => {
    user.client.getInventory()
        .then(result => {
            res.json(result);
        });
});

router.get('/inventory', (req, res, next) => {
    user.getInventory()
        .then(inventory => res.json(inventory));
});

router.post('/inventory/delete', (req:PlayerApi.DeleteReq, res: ExpressCore.Response, next) => {
    user.client
        .recycleInventoryItem(req.body.itemId, req.body.count)
        .then(deleteRes => {
            res.json(deleteRes);
        }).catch(deleteRes => {
            res.status(500).json(deleteRes);
        });
});

router.post('/pokemon/catch', (req: PlayerApi.CatchReq, res: ExpressCore.Response) => {
    let catchablePokemon = req.body.catchablePokemon;
    if (!catchablePokemon) return res.end();

    PlayerApi
        .catchPokemon(catchablePokemon)
        .then(catchRes => {
            let pokemon: POGOProtos.Data.PokemonData =
                {} as POGOProtos.Data.PokemonData;

            if (catchRes.status === POGOProtos.Networking.Responses.CatchPokemonResponse.CatchStatus.CATCH_SUCCESS) {
                user.getInventory()
                    .then(inventory => {
                        pokemon = _.find(inventory.pokemon, p => p.id == catchRes.captured_pokemon_id);
                        res.json({ status: catchRes.status, pokemon: pokemon });
                    });
            }
            else {
                res.json({ status: catchRes.status });
            }
        }).catch(catchRes => {
            res.status(500).json(catchRes);
        });
});

router.post('/pokemon/favorite', (req: PlayerApi.FavoriteReq, res: ExpressCore.Response) => {
    let pokemon = req.body.pokemon;
    if (!pokemon) return res.end();

    PlayerApi
        .favorite(pokemon)
        .then(faviRes => {
            res.json(faviRes.result);
        }).catch(faviRes => {
            res.status(500).json(faviRes);
        });
});

router.post('/pokemon/transport', (req: PlayerApi.FavoriteReq, res: ExpressCore.Response) => {
    let pokemon = req.body.pokemon;
    if (!pokemon) return res.end();

    PlayerApi
        .transport(pokemon)
        .then(transRes => {
            res.json(transRes.result);
        }).catch(transRes => {
            res.status(500).json(transRes);
        });
});

router.post('/pokestop/loot', (req: PlayerApi.LootReq, res: ExpressCore.Response) => {
    let fort = req.body.fort;

    user.client
        .fortDetails(fort.id, fort.latitude, fort.longitude)
        .then(detailRes => {
            user.client.setPosition(fort.latitude, fort.longitude);
            return user.client.fortSearch(fort.id, fort.latitude, fort.longitude);
        }).then(searchRes => {
            res.json(searchRes);
        });
});

namespace PlayerApi {
    export async function catchPokemon(catchablePokemon: POGOProtos.Map.Pokemon.MapPokemon):
        Promise<POGOProtos.Networking.Responses.CatchPokemonResponse> {
        var ball = await getBall();

        user.client.setPosition(catchablePokemon.latitude, catchablePokemon.longitude);

        await encounter(catchablePokemon);

        return user.client.catchPokemon(
            catchablePokemon.encounter_id,
            ball.item_id,
            Math.min(1.95, 1.85 + 0.10 * Math.random()),
            catchablePokemon.spawn_point_id,
            true,
            1,
            1).then(res => {
                if (res.status === POGOProtos.Networking.Responses.CatchPokemonResponse.CatchStatus.CATCH_MISSED ||
                    res.status === POGOProtos.Networking.Responses.CatchPokemonResponse.CatchStatus.CATCH_ESCAPE) {
                    return catchPokemon(catchablePokemon);
                }
                return res;
            });
    }

    async function getBall() {
        return new Promise<POGOProtos.Inventory.Item.ItemData>((resolve, reject) => {
            user.getInventory().then(inventory => {
                // find usable ball
                let ball = inventory.items.find((item, index, ojb) => {
                    return item.item_id === POGOProtos.Inventory.Item.ItemId.ITEM_POKE_BALL ||
                        item.item_id === POGOProtos.Inventory.Item.ItemId.ITEM_GREAT_BALL ||
                        item.item_id === POGOProtos.Inventory.Item.ItemId.ITEM_ULTRA_BALL ||
                        item.item_id === POGOProtos.Inventory.Item.ItemId.ITEM_MASTER_BALL;
                });

                if (!ball) return reject('No usable ball.');
                resolve(ball);
            });
        });
    }

    async function encounter(catchablePokemon: POGOProtos.Map.Pokemon.MapPokemon) {
        return user.client.encounter(catchablePokemon.encounter_id, catchablePokemon.spawn_point_id);
    }

    export async function favorite(pokemon: POGOProtos.Data.PokemonData) {
        return user.client.setFavoritePokemon(pokemon.id, !pokemon.favorite);
    }

    export async function transport(pokemon: POGOProtos.Data.PokemonData) {
        return user.client.releasePokemon(pokemon.id);
    }

    export interface DeleteReq extends ExpressCore.Request {
        body: DeleteBody;
    }

    export interface DeleteBody {
        itemId: number;
        count: number;
    }

    export interface CatchReq extends ExpressCore.Request {
        body: CatchBody;
    }

    export interface CatchBody {
        catchablePokemon: POGOProtos.Map.Pokemon.MapPokemon;
    }

    export interface FavoriteReq extends ExpressCore.Request {
        body: FavoriteBody;
    }

    export interface FavoriteBody {
        pokemon: POGOProtos.Data.PokemonData;
    }

    export interface LootReq extends ExpressCore.Request {
        body: LootBody;
    }

    export interface LootBody {
        fort: POGOProtos.Map.Fort.FortData;
    }
}

export default router;
module.exports = router;