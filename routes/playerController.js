"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
const express = require('express');
const user_1 = require('../src/app/user');
const POGOProtos = require('node-pogo-protos');
const _ = require('lodash');
const router = express.Router();
router.get('/', (req, res, next) => {
    user_1.default.client.getPlayer()
        .then(player => player.player_data)
        .then(playerData => {
        user_1.default.client.getInventory()
            .then(inventory => _.find(inventory.inventory_delta.inventory_items, item => !!item.inventory_item_data.player_stats).inventory_item_data.player_stats)
            .then(playerStatus => {
            res.json(_.merge(playerData, playerStatus));
        });
    });
});
router.get('/delta', (req, res, next) => {
    user_1.default.client.getInventory()
        .then(result => {
        res.json(result);
    });
});
router.get('/inventory', (req, res, next) => {
    user_1.default.getInventory()
        .then(inventory => res.json(inventory));
});
router.post('/inventory/delete', (req, res, next) => {
    user_1.default.client
        .recycleInventoryItem(req.body.itemId, req.body.count)
        .then(deleteRes => {
        res.json(deleteRes);
    }).catch(deleteRes => {
        res.status(500).json(deleteRes);
    });
});
router.post('/pokemon/catch', (req, res) => {
    let catchablePokemon = req.body.catchablePokemon;
    if (!catchablePokemon)
        return res.end();
    PlayerApi
        .catchPokemon(catchablePokemon)
        .then(catchRes => {
        let pokemon = {};
        if (catchRes.status === 1 /* CATCH_SUCCESS */) {
            user_1.default.getInventory()
                .then(inventory => {
                pokemon = _.find(inventory.pokemon, p => p.id == catchRes.captured_pokemon_id);
                res.json({ status: catchRes.status, pokemon: pokemon });
            });
        }
        else {
            res.json({ status: catchRes.status });
        }
    }).catch(catchRes => {
        res.status(500).json(catchRes);
    });
});
router.post('/pokemon/favorite', (req, res) => {
    let pokemon = req.body.pokemon;
    if (!pokemon)
        return res.end();
    PlayerApi
        .favorite(pokemon)
        .then(faviRes => {
        res.json(faviRes.result);
    }).catch(faviRes => {
        res.status(500).json(faviRes);
    });
});
router.post('/pokemon/transport', (req, res) => {
    let pokemon = req.body.pokemon;
    if (!pokemon)
        return res.end();
    PlayerApi
        .transport(pokemon)
        .then(transRes => {
        res.json(transRes.result);
    }).catch(transRes => {
        res.status(500).json(transRes);
    });
});
router.post('/pokestop/loot', (req, res) => {
    let fort = req.body.fort;
    user_1.default.client
        .fortDetails(fort.id, fort.latitude, fort.longitude)
        .then(detailRes => {
        user_1.default.client.setPosition(fort.latitude, fort.longitude);
        return user_1.default.client.fortSearch(fort.id, fort.latitude, fort.longitude);
    }).then(searchRes => {
        res.json(searchRes);
    });
});
var PlayerApi;
(function (PlayerApi) {
    function catchPokemon(catchablePokemon) {
        return __awaiter(this, void 0, Promise, function* () {
            var ball = yield getBall();
            user_1.default.client.setPosition(catchablePokemon.latitude, catchablePokemon.longitude);
            yield encounter(catchablePokemon);
            return user_1.default.client.catchPokemon(catchablePokemon.encounter_id, ball.item_id, Math.min(1.95, 1.85 + 0.10 * Math.random()), catchablePokemon.spawn_point_id, true, 1, 1).then(res => {
                if (res.status === 4 /* CATCH_MISSED */ ||
                    res.status === 2 /* CATCH_ESCAPE */) {
                    return catchPokemon(catchablePokemon);
                }
                return res;
            });
        });
    }
    PlayerApi.catchPokemon = catchPokemon;
    function getBall() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                user_1.default.getInventory().then(inventory => {
                    // find usable ball
                    let ball = inventory.items.find((item, index, ojb) => {
                        return item.item_id === 1 /* ITEM_POKE_BALL */ ||
                            item.item_id === 2 /* ITEM_GREAT_BALL */ ||
                            item.item_id === 3 /* ITEM_ULTRA_BALL */ ||
                            item.item_id === 4 /* ITEM_MASTER_BALL */;
                    });
                    if (!ball)
                        return reject('No usable ball.');
                    resolve(ball);
                });
            });
        });
    }
    function encounter(catchablePokemon) {
        return __awaiter(this, void 0, void 0, function* () {
            return user_1.default.client.encounter(catchablePokemon.encounter_id, catchablePokemon.spawn_point_id);
        });
    }
    function favorite(pokemon) {
        return __awaiter(this, void 0, void 0, function* () {
            return user_1.default.client.setFavoritePokemon(pokemon.id, !pokemon.favorite);
        });
    }
    PlayerApi.favorite = favorite;
    function transport(pokemon) {
        return __awaiter(this, void 0, void 0, function* () {
            return user_1.default.client.releasePokemon(pokemon.id);
        });
    }
    PlayerApi.transport = transport;
})(PlayerApi || (PlayerApi = {}));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = router;
module.exports = router;
