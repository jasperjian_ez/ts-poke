"use strict";
const express = require('express');
const user_1 = require('../src/app/user');
const pogobuf = require('pogobuf');
const bluebird = require('bluebird');
const POGOProtos = require('node-pogo-protos');
const router = express.Router();
router.get('/nearby/:coords', (req, res, next) => {
    let latitude = +req.params.coords.split(',')[0];
    let longitude = +req.params.coords.split(',')[1];
    user_1.default.client.setPosition(latitude, longitude);
    let cellIDs = pogobuf.Utils.getCellIDs(latitude, longitude);
    user_1.default.client
        .getMapObjects(cellIDs, Array(cellIDs.length).fill(0))
        .then(mapObjects => {
        let nearybyResponse = {
            pokestops: [],
            gyms: [],
            catchablePokemons: []
        };
        mapObjects.map_cells.forEach(cell => {
            cell.forts.forEach(fort => {
                if (fort.type == 1 /* CHECKPOINT */) {
                    nearybyResponse.pokestops.push(fort);
                }
                else if (fort.type == 0 /* GYM */) {
                    nearybyResponse.gyms.push(fort);
                }
            });
            nearybyResponse.catchablePokemons = nearybyResponse.catchablePokemons.concat(cell.catchable_pokemons);
        });
        res.json(nearybyResponse);
    });
});
router.get('/fort/:fort', (req, res, next) => {
    let fortId = req.params.fort.split(',')[0];
    let latitude = +req.params.fort.split(',')[1];
    let longitude = +req.params.fort.split(',')[2];
    user_1.default.client
        .fortDetails(fortId, latitude, longitude)
        .then(detailRes => {
        res.json(detailRes);
    });
});
router.get('/nearby/:coords', (req, res, next) => {
    var latitude = +req.params.coords.split(',')[0];
    var longitude = +req.params.coords.split(',')[1];
    user_1.default.client.setPosition(latitude, longitude);
    var cellIDs = pogobuf.Utils.getCellIDs(latitude, longitude);
    bluebird.resolve(user_1.default.client.getMapObjects(cellIDs, Array(cellIDs.length).fill(0)))
        .then(mapObjects => {
        return mapObjects.map_cells;
    }).each((cell) => {
        console.log('Cell ' + cell.s2_cell_id.toString());
        console.log('Has ' + cell.catchable_pokemons.length + ' catchable Pokemon');
        return bluebird.resolve(cell.catchable_pokemons).each((catchablePokemon) => {
            // console.log(' - A ' + pogobuf.Utils.getEnumKeyByValue(POGOProtos.Enums.PokemonId,
            //     catchablePokemon.pokemon_id) + ' is asking you to catch it.');
        });
    }).then((value) => {
        res.json(value);
    });
});
router.get('/nearby/:coords', (req, res, next) => {
    let latitude = +req.params.coords.split(',')[0];
    let longitude = +req.params.coords.split(',')[1];
    user_1.default.client.setPosition(latitude, longitude);
    let cellIDs = pogobuf.Utils.getCellIDs(latitude, longitude);
    user_1.default.client
        .getMapObjects(cellIDs, Array(cellIDs.length).fill(0))
        .then(mapObjects => {
        user_1.default.client.batchStart();
        mapObjects.map_cells.map(cell => cell.forts)
            .reduce((a, b) => a.concat(b))
            .filter(fort => fort.type == 1 /* CHECKPOINT */)
            .forEach(fort => {
            user_1.default.client.fortDetails(fort.id, fort.latitude, fort.longitude);
        });
        return user_1.default.client.batchCall();
    })
        .then(stops => {
    });
});
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = router;
module.exports = router;
