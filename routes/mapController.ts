import * as express from 'express';
import * as ExpressCore from 'express-serve-static-core';
import user from '../src/app/user';
import * as pogobuf from 'pogobuf';
import * as bluebird from 'bluebird';
import * as POGOProtos from 'node-pogo-protos';

const router = express.Router();

router.get('/nearby/:coords', (req, res, next) => {
    let latitude = +req.params.coords.split(',')[0];
    let longitude = +req.params.coords.split(',')[1];

    user.client.setPosition(latitude, longitude);

    let cellIDs = pogobuf.Utils.getCellIDs(latitude, longitude);

    user.client
        .getMapObjects(cellIDs, Array(cellIDs.length).fill(0))
        .then(mapObjects => {
            let nearybyResponse: NearbyResponse = {
                pokestops: [],
                gyms: [],
                catchablePokemons: []
            };

            mapObjects.map_cells.forEach(cell => {
                cell.forts.forEach(fort => {
                    if (fort.type == POGOProtos.Map.Fort.FortType.CHECKPOINT) {
                        nearybyResponse.pokestops.push(fort);
                    }
                    else if (fort.type == POGOProtos.Map.Fort.FortType.GYM) {
                        nearybyResponse.gyms.push(fort)
                    }
                });

                nearybyResponse.catchablePokemons = nearybyResponse.catchablePokemons.concat(cell.catchable_pokemons);
            });

            res.json(nearybyResponse);
        });

});

router.get('/fort/:fort', (req: FortRequest, res: ExpressCore.Response, next: ExpressCore.NextFunction) => {
    let fortId = req.params.fort.split(',')[0];
    let latitude = +req.params.fort.split(',')[1];
    let longitude = +req.params.fort.split(',')[2];

    user.client
        .fortDetails(fortId, latitude, longitude)
        .then(detailRes => {
            res.json(detailRes);
        })
})

router.get('/nearby/:coords', (req, res, next) => {
    var latitude = +req.params.coords.split(',')[0];
    var longitude = +req.params.coords.split(',')[1];

    user.client.setPosition(latitude, longitude);

    var cellIDs = pogobuf.Utils.getCellIDs(latitude, longitude);

    bluebird.resolve(
        user.client.getMapObjects(cellIDs, Array(cellIDs.length).fill(0)))
        .then(mapObjects => {
            return mapObjects.map_cells;
        }).each((cell: POGOProtos.Map.MapCell) => {
            console.log('Cell ' + cell.s2_cell_id.toString());
            console.log('Has ' + cell.catchable_pokemons.length + ' catchable Pokemon');
            return bluebird.resolve(cell.catchable_pokemons).each((catchablePokemon: POGOProtos.Map.Pokemon.MapPokemon) => {
                // console.log(' - A ' + pogobuf.Utils.getEnumKeyByValue(POGOProtos.Enums.PokemonId,
                //     catchablePokemon.pokemon_id) + ' is asking you to catch it.');
            });
        }).then((value) => {
            res.json(value);
        });
});

router.get('/nearby/:coords', (req, res, next) => {
    let latitude = +req.params.coords.split(',')[0];
    let longitude = +req.params.coords.split(',')[1];

    user.client.setPosition(latitude, longitude);

    let cellIDs = pogobuf.Utils.getCellIDs(latitude, longitude);

    user.client
        .getMapObjects(cellIDs, Array(cellIDs.length).fill(0))
        .then(mapObjects => {
            user.client.batchStart();

            mapObjects.map_cells.map(cell => cell.forts)
                .reduce((a, b) => a.concat(b))
                .filter(fort => fort.type == POGOProtos.Map.Fort.FortType.CHECKPOINT)
                .forEach(fort => {
                    user.client.fortDetails(fort.id, fort.latitude, fort.longitude);
                });

            return user.client.batchCall();
        })
        .then(stops => {
        });
});

interface FortRequest extends ExpressCore.Request {
    params: FortRequestParams;
}

interface FortRequestParams {
    fort: string;
}

interface NearbyResponse {
    pokestops?: POGOProtos.Map.Fort.FortData[];
    gyms?: POGOProtos.Map.Fort.FortData[];
    catchablePokemons?: POGOProtos.Map.Pokemon.MapPokemon[];
}

export default router;
module.exports = router;