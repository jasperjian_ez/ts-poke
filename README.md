# TypeScript Pokemap

## Preparation

TypeScript Typings installed.

```bash
npm i -g typescript typings
```

## Account Settings

config.ts

```typescript
export default {
    username: 'YOUR_DEV_USERNAME',
    password: 'YOUR_DEV_PASSWORD'
}
```

## Build

```bash
npm i
# link global typescript package
npm link typescript
# starting typescript compiler in watch mode
tsc
# starting server
npm start
```

Go to <http://localhost:3000>